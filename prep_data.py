#!/usr/bin/env python

import sys
import csv
import json
import glob
import math
import xlrd
import fiona
import shapely
import openpyxl
import collections
from shapely.geometry import Point, Polygon
from shapely.ops import nearest_points

def csv_dict(csv_file):
    csv_data = csv.reader(csv_file)
    dict_data = {}
    for row in csv_data:
        dict_data[row[0]] = ','.join([str(x) for x in row[1:]])
    return dict_data

def csv_list_tuple(csv_file):
    csv_data = csv.reader(csv_file)
    list_data = []
    for row in csv_data:
        list_data.append(tuple(row))
    return list_data

def csv_list_dict(csv_file):
    csv_data = csv.reader(csv_file)
    header = next(csv_data)
    assert all([isinstance(x, str) for x in header]), "Header (first row) includes not-string elemnets"
    header = [x.lower() for x in header]
    ncol = len(header)
    list_dict = []
    for row in csv_data:
        if len(row) == ncol:
            mm = {}
            for i in range(ncol):
                mm[header[i]] = row[i]
            list_dict.append(mm)
    return list_dict

def select_state_config(list_dict,config):
    if config['state'] == ['ALL']:
        return list_dict
    else:
        select = []
        for k in list_dict:
            if k['state'] in config['state']:
                select.append(k)
        return select

def state_config(dict_file,config):
    if config['state'] == ['ALL']:
        return dict_file
    else:
        select = dict_file.copy()
        for scd in dict_file:
            if scd not in config['state']:
                del select[scd]
        return select

def select_att_config(dict_,config):
    select = {}
    for i in config['attribute_cd']:
        select[str(i)] = dict_[str(i)]
    return select

def select_uniq_id(list_dict,id_name):
    id_list = []
    for k in list_dict:
        id_list.append(k[id_name])
    id_list = list(set(id_list))
    uniq = []
    for j in list_dict:
        if j[id_name] in id_list:
            uniq.append(j)
            id_list.remove(j[id_name])
    return uniq

def dict_csv(dict_data,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    for j in dict_data:
        row = [j] + [dict_data[j]]
        fcsv.writerow(row)

def list_dict_csv(list_dict,keys,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    fcsv.writerow(keys)
    for i in list_dict:
        for j in keys:
            if j in i.keys():
                if type(i[j]) is list:
                    i[j] = ','.join(i[j])
            else:
                i[j] = 'NA'
        row = [i[x] for x in keys]
        fcsv.writerow(row)

def list_dict_panel(list_dict,keys,config,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    fcsv.writerow(keys + ['year'] + ['att_%s' % x for x in config['attribute_cd']])
    for i in list_dict:
        for j in keys:
            if j in i.keys():
                if type(i[j]) is list:
                    i[j] = ','.join(i[j])
            else:
                i[j] = 'NA'
        row = [i[x] for x in keys]
        for y in config['year']:
            att_yr = []
            att_yr.extend(['%s_%s' % (x,y) for x in config['attribute_cd']])
            for ay in att_yr:
                if ay not in i.keys():
                    i[ay] = 'NA'
            fcsv.writerow(row + [y] + [i[x] for x in att_yr])

def list_dict_panel_power(list_dict,keys,config,csv_output):
    l = len_period(config['year'])
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    fcsv.writerow(keys + ['year','net_generation','net_period_generation'])
    for i in list_dict:
        for j in keys:
            if j in i.keys():
                if type(i[j]) is list:
                    i[j] = ','.join(i[j])
            else:
                i[j] = 'NA'
        row = [i[x] for x in keys]
        for y in config['year']:
            fcsv.writerow(row + [y] + [i['net_generation_%s' % y]] + [round(sum([float(i['net_generation_%s' % x]) for x in range(y - l + 1, y + 1)]), 4)])

def xl_csv(xl_file,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    if "xlsx" in str(xl_file):
        wb = openpyxl.load_workbook(xl_file)
        for x in wb.sheetnames:
            ws = wb[x]
            fcsv.writerow("sheet name: %s" % x)
            for row_cells in ws.iter_rows():
                fcsv.writerow([cell.value for cell in row_cells])
    else:
        wb = xlrd.open_workbook(xl_file)
        for x in wb.sheet_names():
            ws = wb.sheet_by_name(x)
            fcsv.writerow("sheet name: %s" % x)
            for rowx in range(ws.nrows):
                row_ = ws.row_values(rowx)
                fcsv.writerow(row_)

def xl_sheet_csv(xl_file,sheet_name,csv_output):
    fcsv = csv.writer(csv_output, lineterminator = '\n')
    if "xlsx" in str(xl_file):
        wb = openpyxl.load_workbook(xl_file)
        ws = wb[sheet_name]
        for row_cells in ws.iter_rows():
            fcsv.writerow([cell.value for cell in row_cells])
    else:
        wb = xlrd.open_workbook(xl_file)
        ws = wb.sheet_by_name(sheet_name)
        for rowx in range(ws.nrows):
            row_ = ws.row_values(rowx)
            fcsv.writerow(row_)

def xl_sheetnames(xl_file):
    if "xlsx" in str(xl_file):
        wb =  openpyxl.load_workbook(xl_file)
        return wb.sheetnames
    else:
        wb = xlrd.open_workbook(xl_file)
        return wb.sheet_names()

def len_period(x):
    if len(x) > 2:
        l = {x[i+1]-x[i] for i in range(len(x)-1)}
        assert len(l) == 1, "Error: multiple year-step. Select years with a constant step e.g. every year or every two years and so on. Update years in the config file."
        l = l.pop()
        assert min(x) - l >= 2001, "Error: beyond the scope. The oldest EIA data is for 2001, minimum year minus length of the period (step) should be greater than or equal 2001."
        return l
    else:
        return 0

def haversine(point1, point2): # point = (x,y) = (lon,lat)
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(math.radians,[point1[0], point1[1], point2[0], point2[1]])
    
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
    c = 2 * math.asin(math.sqrt(a))
    r = 3956 # Radius of earth in mile. Use 6371 for km
    return c * r

def dist_point_polyg(points,shape_file):
    dist_dict = collections.defaultdict(dict)
    for i in points:
        unit_id = i['unit_id']
        point_ = Point(float(i['lon']),float(i['lat']))
        for j in shape_file:
            state_name = j['properties']['NAME']
            polyg = shapely.geometry.shape(j['geometry'])
            nearest_pt, pt = nearest_points(polyg, point_)
            nearest_coord = (nearest_pt.x,nearest_pt.y)
            pt_coord = (pt.x, pt.y)
            dist_mile = haversine(nearest_coord,pt_coord)
            dist_dict[unit_id][state_name] = dist_mile
    return dist_dict

if __name__=='__main__':
    ## Read config
    config = json.load(open(sys.argv[1]))
    year = config['year']
    state = config['state']
    
    assert "DC" not in state, "FIA does not include data for DC. Remove DC from the listed states in the config file."
    
    ## FIA attributes
    with open('./other_data/attributes_all.csv', 'r') as att:
        cd_att_all = csv_list_dict(att)
    
    ## FIA attributes number and name
    cd_att = {}
    for atr in cd_att_all:
        cd_att[atr['attribute_nbr']] = atr['attribute_descr']
    
    ## JSON and CSV outputs of selected FIA attributes (number and name)
    att_select = select_att_config(cd_att,config)
    with open('attributes.json', 'w') as atj:
        json.dump(att_select, atj)
    
    with open('attributes.csv', 'w') as atc:
        dict_csv(att_select,atc)
    
    ## Census state codes
    with open('./state_codes.csv', 'r') as cd:
        state_cd = csv_dict(cd)
    
    with open('./state_abb.csv', 'r') as ab:
        state_abb = csv_dict(ab)

    abb_state = {v:k for k,v in state_abb.items()}
    
    ## Power plants
    ## ------------
    with open('./power_info.csv', 'r') as po:
        power = csv_list_dict(po)
    
    ### Seletct power plants based on the config
    power_select = select_state_config(power,config)
    
    for p in power_select:
        p['unit_id'] = p['plant_code']
        p['state_name'] = abb_state[p['state']]
        p['state_cd'] = state_cd[p['state']]
    
    ### Read state shape file and find nearest distance from each point to each state
    with fiona.open(glob.glob('./other_data/shape_state/*state*.shp').pop()) as shp:
        dist_data = dist_point_polyg(power_select,shp)
    
    for p in power_select:
        p['neighbors_name'] = [x for x in dist_data[p['unit_id']].keys() if dist_data[p['unit_id']][x] < float(p['radius'])]
        p['neighbors_name'].remove(p['state_name'])
        p['neighbors'] = [state_abb[x] for x in p['neighbors_name']]
        p['neighbors_cd'] = [state_cd[x] for x in p['neighbors']]
    
    ### JSON output of unique power plants
    power_uniq = select_uniq_id(power_select,'unit_id')
    with open('./power.json', 'w') as fj:
        json.dump(power_uniq,fj)
    
    ### Update the CSV input
    keys = list(power_select[0].keys())
    with open ('./power_info.csv', 'w') as fc:
        list_dict_csv(power_select,keys,fc)
    
    ## Power plants generation projection
    ## ----------------------------------
    with open('./power_projection.csv', 'r') as bp:
        power_proj = csv_list_dict(bp)
    
    for p in power_proj:
        if p['state'].title() in state_abb.keys():
            p['unit_id'] = str(round(float(p['latitude']),3)).replace('.','') + str(round(float(p['longitude']),3)).replace('.','').replace('-','')
            p['state_name'] = p['state'].title()
            p['state'] = state_abb[p['state_name']]
            p['casename'] = p['casename'].strip()
            p['year'] = int(p['year'].strip()[1:])
    
    ### Seletct power plants based on the config
    power_proj_select = select_state_config(power_proj,config)
    
    ### Update the CSV input
    keys = list(power_proj_select[0].keys())
    with open ('./power_projection_info.csv', 'w') as fc:
        list_dict_csv(power_proj_select,keys,fc)
    
    ## Pellet plants
    ## -------------
    with open('./pellet_info.csv', 'r') as pe:
        pellet = csv_list_dict(pe)
    
    header = pellet[0].keys()
    assert all(x in header for x in ['lat','lon','state','capacity']), "Error: 'pellet_info.csv' does not include required filds, 'lat', 'lon', 'state' and 'capacity'."
    
    ### Seletct pellet plants based on the config
    pellet_select = select_state_config(pellet,config)
    
    pellet_select_ = []
    for p in pellet_select:
        if int(p['opened']) <= max(year) and int(p['retired']) >= min(year):
            pellet_select_.append(p)
    
    for p in pellet_select_:
        p['state_cd'] = state_cd[p['state']]
        if float(p['capacity']) >= config['pellet_plant_cap_ton']:
            p['radius'] = max(config['pellet_plant_radius_mile'])
        else:
            p['radius'] = min(config['pellet_plant_radius_mile'])
    
    ### Update the CSV input
    keys = list(pellet_select_[0].keys())
    with open ('./pellet_info.csv', 'w') as fc:
        list_dict_csv(pellet_select_,keys,fc)
    
    ## Counties population for the selected years
    ## ------------------------------------------
    with open('./other_data/pop_2000_2009.csv', 'r') as pop_00:
        pop_2000 = csv_list_dict(pop_00) 
    pop_2000_dict = {}
    for i in pop_2000:
        pop_2000_dict['%s%s' % (i['state'],i['county'])] = i
    
    with open('./other_data/pop_2010_2020.csv', 'r') as pop_10:
        pop_2010 = csv_list_dict(pop_10)
    pop_2010_dict = {}
    for i in pop_2010:
        pop_2010_dict['%s%s' % (i['state'],i['county'])] = i
    
    pop_ = []
    for i in pop_2010_dict.keys():
        if i in pop_2000_dict.keys():
            assert pop_2000_dict[i]['county'] == pop_2010_dict[i]['county']
            pop_2010_dict[i].update(pop_2000_dict[i])
            pop_.append(pop_2010_dict[i])
        else:
            print("State_county %s is not available in both datasets" % i)
            
    pop_keys = ['state', 'county', 'stname', 'ctyname'] + ['popestimate%s' % x for x in config['year']] 
    with open('./population_info.csv', 'w') as pop_out:
        list_dict_csv(pop_,pop_keys,pop_out)
