#!/usr/bin/env python

import os
import re
import sys
import csv
import glob
import time
import json
import xlrd
import openpyxl
import collections
import prep_data

config = json.load(open(sys.argv[1]))
year = config['year']
l = prep_data.len_period(year)
rng = range(min(year)-l+1,max(year)+1)
plants = []

for y in reversed(rng):
    ## Select coal power plants
    os.system(f"""
    if [ {y} -ge 2004 ]; then
    cat ${{OTHER}}/power_plants/generators_{y}.csv | grep -v 'Form EIA-860 Data' | grep -P 'STATUS|Status|,ANT,|,BIT,|,LIG,|,SGC,|,SUB,|,WC,|,RC,' | grep -Pv ',BLQ,|,WDL,|,WDS,' | sed 's/\.0,/,/g' | sed '1 s/ /_/g' > ${{OTHER}}/power_plants/coal_generators_{y}.csv
    fi
    cat ${{OTHER}}/power_plants/generations_{y}.csv | grep -P 'Plant Id|Plant Name|Reported|Fuel Type|Quantity|Net Gen|Netgen|MMB|Consump|YEAR' | grep -v ',,,' | tr '\n' ' ' | tr -d '"' | sed 's/ /_/g' | sed 's/.$//' > ${{OTHER}}/power_plants/coal_generation_{y}.csv
    cat ${{OTHER}}/power_plants/generations_{y}.csv | grep -P ',ANT,|,BIT,|,LIG,|,SGC,|,SUB,|,WC,|,RC,' | grep -Pv ',BLQ,|,WDL,|,WDS,' | sed 's/\.0,/,/g' >> ${{OTHER}}/power_plants/coal_generation_{y}.csv
    """)

    ## Prepare power_data file
    ### Coal generators EIA-850 - GEN
    if int(y) < 2004:
        t = max(year)
    else:
        t = y
        with open(f"./other_data/power_plants/coal_generators_{t}.csv", 'r') as gb:
            generator = prep_data.csv_list_dict(gb)
    
    ### Plant locations EIA-860 - PLANT (lat/lon only is available in years after 2012) 
    if int(y) < 2012:
        k = max(year)
    else:
        k = y
    with open(f"./other_data/power_plants/plants_{k}.csv", 'r') as pb:
        pwp = prep_data.csv_list_dict(pb)
    
    ### Coal generartion EIA-923
    with open(f"./other_data/power_plants/coal_generation_{y}.csv" , 'r') as gc:
        generation = prep_data.csv_list_dict(gc)
    
    ### Rename columns in GEN EIA-860 to the same names for all yeras
    coal_plant_code = []
    for i in generator:
        pc_ = re.findall('plant_code|plntcode',str(i.keys()))
        oy_ = re.findall('operating_year|insvyear',str(i.keys()))
        i['plant_code'] = i.pop(pc_[0])
        i['operating_year'] = i.pop(oy_[0])
        coal_plant_code.append(i['plant_code'])
    coal_plant_code = set(coal_plant_code)
    
    ### Find net annual generation for each power plant from  coal fuels (ANT, BIT, LIG, SGC, SUB, WC, RC) from EIA-923
    generation_dict = collections.defaultdict(list)
    for i in generation:
        generation_dict[i['plant_id']].append(i['net_generation_(megawatthours)'])
    
    for i in generation_dict:
        generation_dict[i] = round(sum([float(x) for x in generation_dict[i]]),4)
    
    ### Unique power plants in the GEN dataset
    uniq_plants = prep_data.select_uniq_id(generator,'plant_code')
    
    ### Add net generation to the unique coal power plants
    for i in uniq_plants:
        if i['plant_code'] in generation_dict.keys():
            i['net_generation'] = generation_dict[i['plant_code']]
        else:
            i['net_generation'] = 0
    
    ### Create a dict of unique PLANTs to add opened year and lat/lon
    dict_uniq_plants = {}
    for i in uniq_plants:
        dict_uniq_plants[i['plant_code']] = i.copy()
        dict_uniq_plants[i['plant_code']]['operating_year'] = []
    
    for i in generator:
        dict_uniq_plants[i['plant_code']]['operating_year'].append(i['operating_year'])
    
    for i in pwp:
        if i['plant_code'] in coal_plant_code:
            dict_uniq_plants[i['plant_code']]['lat'] = i['latitude']
            dict_uniq_plants[i['plant_code']]['lon'] = i['longitude']
            dict_uniq_plants[i['plant_code']]['nerc_region'] = i['nerc_region']
    
    for i in dict_uniq_plants:
        dict_uniq_plants[i]['cd_year'] = str(i) + str(y)
        dict_uniq_plants[i]['opened'] = min([int(x) for x in dict_uniq_plants[i]['operating_year']])
        if dict_uniq_plants[i]['net_generation'] > 0 and 'lat' in dict_uniq_plants[i].keys():
            plants.append(dict_uniq_plants[i])

## Dictionary of power plants
dict_plants_all = {}
for i in plants:
    dict_plants_all[i['cd_year']] = i.copy()

## Unique power plants
uniq_plants_all = prep_data.select_uniq_id(plants,'plant_code')

for i in uniq_plants_all:
    i['opened'] = []
    i['retired'] = []
    for y in rng:
        if str(i['plant_code']) + str(y) in dict_plants_all.keys():
            i['opened'].append(dict_plants_all[str(i['plant_code']) + str(y)]['opened'])
            i['retired'].append(y)
            i[f"net_generation_{y}"] = dict_plants_all[str(i['plant_code']) + str(y)]['net_generation']
        else:
            i[f"net_generation_{y}"] = 0
    i['opened'] = min(i['opened'])
    i['retired'] = max(i['retired'])
    i['type'] = 'coal'
    i['total_generation'] = round(sum([float(i[f"net_generation_{x}"]) for x in rng]), 4)
    if i['total_generation'] >= config['power_plant_cap_mwh']:
        i['radius'] = max(config['power_plant_radius_mile'])
    else:
        i['radius'] = min(config['power_plant_radius_mile'])

## Select states based on the config file
plants_select = prep_data.select_state_config(uniq_plants_all,config)

## Panel CSV output
keys = ['plant_code','type','state','lat','lon','nerc_region','opened','retired','total_generation','radius']
with open('./other_data/power_info_coal.csv', 'w') as fp:
    prep_data.list_dict_panel_power(plants_select,keys,config,fp)

## Adding coal-firing plants to biopower plants
os.system("""
sed -i '1 s/ /_/g; 1 s/(//g; 1 s/)//g; 1 s/?//g; 1 s/\//_/g' ${OTHER}/power_info_coal.csv
""")
