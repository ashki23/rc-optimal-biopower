#!/bin/bash

#SBATCH --job-name=Stat
#SBATCH --cpus-per-task=10
#SBATCH --mem=64G

## Local environments
source environment.sh

## Activate R environment
conda deactivate
conda activate ./optimal_r_env

## Prepare inputs for the regression analysis
cp $(ls -t ${OUTPUT}/power-panel*.csv | head -n 1) ${PROJ_HOME}/panel_power.csv
cat ${PROJ_HOME}/attributes.csv | sed 's/ (.*)//g' | sed 's/, in /,/g' | sed 's/, on /,/g' | sed 's/Aboveground and belowground carbon/Carbon/g' | tr -d \" > ${PROJ_HOME}/att_unit.csv

echo ============ Data analysis with R ========== $(hostname) $(date)

## Run R model
rm *.RData
for dist in {150,200,250}; do # per mile
    Rscript statistical_analysis.R ${dist} > Routputs_stat_${dist}.out || return || exit
    sleep 1
    mv Rplots.pdf Rplots_stat_${dist}.pdf
    
    echo ==== Optimization with GAMS - $dist ==== $(hostname) $(date)
    
    ## Run gmas models
    rm jobid-gams-${dist}.log
    python run_gams.py config.json ${dist}
    sleep 2
    
    echo === Forecast analysis with R - $dist === $(hostname) $(date)
    
    ## Run R model
    if [ `jq ."job_number_max" config.json` -eq 1 ]; then
	source run_model_2.sh ${dist}; else
	sbatch --dependency=afterok:`cat ${PROJ_HOME}/jobid-gams-${dist}.log | tr '\n' ',' | grep -Po '.*(?=,$)'` run_model_2.sh ${dist};
    fi
done
