#!/bin/bash

export PROJ_HOME=${PWD}
export OUTPUT=${PWD}/outputs
export OTHER=${PWD}/other_data
export FIA=${PWD}/fia_data
install -dvp ${OUTPUT}
install -dvp ${OTHER}/power_plants
install -dvp ${FIA}/survey
MYHOME=${HOME}

## Install Miniconda
if [ ! -d miniconda ]; then 
source bootstrap.sh
fi

## Initiate conda
export HOME=${PROJ_HOME}
export PATH=${PROJ_HOME}/miniconda/bin/:${PATH}
source ./miniconda/etc/profile.d/conda.sh

## Deactivat active envs
conda deactivate

echo ============ Create Conda envs ============= $(hostname) $(date) 

## Create local environments
if [ ! -d optimal_py_env ]; then
## Including: python openpyxl xlrd jq shapely fiona
conda create --yes --prefix ./optimal_py_env --file ./optimal_py_env.txt
fi

if [ ! -d optimal_r_env ]; then
## Including: r-base r-nlme r-plm r-doparallel r-foreach r-sf=1.0_2 r-rjson r-knitr r-ggplot2
conda create --yes --prefix ./optimal_r_env --channel conda-forge --file ./optimal_r_env.txt
fi

## Activate and update Python environment
conda activate ./optimal_py_env
export HOME=${MYHOME}
sleep 3
