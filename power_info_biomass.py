#!/usr/bin/env python

import os
import re
import sys
import csv
import glob
import json 
import xlrd
import openpyxl
import collections
import prep_data

config = json.load(open(sys.argv[1]))
year = config['year']
l = prep_data.len_period(year)
rng = range(min(year) - l + 1, max(year) + 1)

plants = []
for y in reversed(rng):
    print('************* EIA-860/923:', y, '***************')
    os.system(f"""
    wget -c -nv --adjust-extension https://www.eia.gov/electricity/data/eia860/xls/eia860{y}.zip -P ${{OTHER}}/
    wget -c -nv --adjust-extension https://www.eia.gov/electricity/data/eia860/archive/xls/eia860{y}.zip -P ${{OTHER}}/
    wget -c -nv --adjust-extension https://www.eia.gov/electricity/data/eia923/xls/f923_{y}.zip -P ${{OTHER}}/
    wget -c -nv --adjust-extension https://www.eia.gov/electricity/data/eia923/archive/xls/f923_{y}.zip -P ${{OTHER}}/
    wget -c -nv --adjust-extension https://www.eia.gov/electricity/data/eia923/archive/xls/f906920_{y}.zip -P ${{OTHER}}/
    rm ${{OTHER}}/*.html
    unzip -n ${{OTHER}}/eia860{y}.zip -d ${{OTHER}}/eia860_{y}
    unzip -n ${{OTHER}}/f9*_{y}.zip -d ${{OTHER}}/eia923_{y}
    if [ {y} -ge 2004 ]; then
    cp "$(find ${{OTHER}}/eia860_{y}/ -iname "*Plant*.xlsx")" ${{OTHER}}/power_plants/plants_{y}.xlsx
    cp "$(find ${{OTHER}}/eia860_{y}/ -iname "*Plant*.xls")" ${{OTHER}}/power_plants/plants_{y}.xls
    
    cp "$(find ${{OTHER}}/eia860_{y}/ -iname "*Gen*.xlsx" | sort | head -n 1)" ${{OTHER}}/power_plants/generators_{y}.xlsx
    cp "$(find ${{OTHER}}/eia860_{y}/ -iname "*Gen*.xls" | sort | head -n 1)" ${{OTHER}}/power_plants/generators_{y}.xls
    fi
    cp "$(find ${{OTHER}}/eia923_{y}/ -iname "*.xlsx" | grep -P "f906920|eia923D|2_3")" ${{OTHER}}/power_plants/generations_{y}.xlsx
    cp "$(find ${{OTHER}}/eia923_{y}/ -iname "*.xls" | grep -P "f906920|eia923D|2_3")" ${{OTHER}}/power_plants/generations_{y}.xls
    """)
    
    if y >= 2004:
        plant = glob.glob(f"./other_data/power_plants/plants_{y}.xl*")[0]
        print(plant)
        sheet_names = prep_data.xl_sheetnames(plant)
        sheet = re.findall('[Pp]lant\w*',str(sheet_names))
        with open(f"./other_data/power_plants/plants_data_{y}.csv", 'w') as plt:
            prep_data.xl_sheet_csv(plant,sheet[0],plt)
        
        generator = glob.glob(f"./other_data/power_plants/generators_{y}.xl*")[0]
        print(generator)
        sheet_names = prep_data.xl_sheetnames(generator)
        sheet = re.findall('[Ee]xist|[Oo]perable|Gen\w*',str(sheet_names))
        with open(f"./other_data/power_plants/generators_{y}.csv", 'w') as opg:
            prep_data.xl_sheet_csv(generator,sheet[0],opg)
        
    annual_gen_mwh = glob.glob(f"./other_data/power_plants/generations_{y}.xl*")[0]
    print(annual_gen_mwh)
    with open(f"./other_data/power_plants/generations_{y}.csv", 'w') as agm:
        prep_data.xl_sheet_csv(annual_gen_mwh,'Page 1 Generation and Fuel Data',agm)
    
    ## Select bio power plants
    os.system(f"""
if [ {y} -ge 2004 ]; then
cat ${{OTHER}}/power_plants/plants_data_{y}.csv | grep -v 'Form EIA-860 Data' | sed 's/\.0,/,/g' | sed '1 s/ /_/g' > ${{OTHER}}/power_plants/plants_{y}.csv
cat ${{OTHER}}/power_plants/generators_{y}.csv | grep -v 'Form EIA-860 Data' | grep -P 'STATUS|Status|,BLQ,|,WDL,|,WDS,' | sed 's/\.0,/,/g' | sed '1 s/ /_/g' > ${{OTHER}}/power_plants/bio_generators_{y}.csv
fi
cat ${{OTHER}}/power_plants/generations_{y}.csv | grep -P 'Plant Id|Plant Name|Reported|Fuel Type|Quantity|Net Gen|Netgen|MMB|Consump|YEAR' | grep -v ',,,' | tr '\n' ' ' | tr -d '"' | sed 's/ /_/g' | sed 's/.$//' > ${{OTHER}}/power_plants/bio_generation_{y}.csv
cat ${{OTHER}}/power_plants/generations_{y}.csv | grep -P ',BLQ,|,WDL,|,WDS,' | sed 's/\.0,/,/g' >> ${{OTHER}}/power_plants/bio_generation_{y}.csv
    """)
    
    ## Prepare power_data file
    ### Bio generators EIA-860 - GEN
    if int(y) < 2004:
        t = min([x for x in year if int(x) >= 2004], key = lambda x:abs(x-int(y)))
    else:
        t = y
    with open(f"./other_data/power_plants/bio_generators_{t}.csv", 'r') as gb:
        generator = prep_data.csv_list_dict(gb)
    
    ### Plant locations EIA-860 - PLANT (lat/lon only is available in years after 2012) 
    if int(y) < 2012:
        k = min([x for x in year if int(x) >= 2012], key = lambda x:abs(x-int(y)))
    else:
        k = y
    with open(f"./other_data/power_plants/plants_{k}.csv", 'r') as pb:
        pwp = prep_data.csv_list_dict(pb)
    
    ### Bio generartion EIA-923
    with open(f"./other_data/power_plants/bio_generation_{y}.csv" , 'r') as gc:
        generation = prep_data.csv_list_dict(gc)
    
    ### Rename columns in GEN EIA-860 to the same names for all yeras
    bio_plant_code = []
    for i in generator:
        pc_ = re.findall('plant_code|plntcode',str(i.keys()))
        oy_ = re.findall('operating_year|insvyear',str(i.keys()))
        i['plant_code'] = i.pop(pc_[0])
        i['operating_year'] = i.pop(oy_[0])
        bio_plant_code.append(i['plant_code'])
    bio_plant_code = set(bio_plant_code)
    
    ### Find net annual generation for each power plant from  bio fuels (WDS, WDL and BLQ) from EIA-923
    generation_dict = collections.defaultdict(list)
    for i in generation:
        generation_dict[i['plant_id']].append(i['net_generation_(megawatthours)'])
    
    for i in generation_dict:
        generation_dict[i] = round(sum([float(x) for x in generation_dict[i]]),4)
    
    ### Unique power plants in the GEN dataset
    uniq_plants = prep_data.select_uniq_id(generator,'plant_code')
    
    ### Add net generation to the unique bio power plants
    for i in uniq_plants:
        if i['plant_code'] in generation_dict.keys():
            i['net_generation'] = generation_dict[i['plant_code']]
        else:
            i['net_generation'] = 0
    
    ### Create a dict of unique PLANTs to add opened year and lat/lon
    dict_uniq_plants = {}
    for i in uniq_plants:
        dict_uniq_plants[i['plant_code']] = i.copy()
        dict_uniq_plants[i['plant_code']]['operating_year'] = []
    
    for i in generator:
        dict_uniq_plants[i['plant_code']]['operating_year'].append(i['operating_year'])
    
    for i in pwp:
        if i['plant_code'] in bio_plant_code:
            dict_uniq_plants[i['plant_code']]['lat'] = i['latitude']
            dict_uniq_plants[i['plant_code']]['lon'] = i['longitude']
            dict_uniq_plants[i['plant_code']]['nerc_region'] = i['nerc_region']
    
    for i in dict_uniq_plants:
        dict_uniq_plants[i]['cd_year'] = str(i) + str(y)
        dict_uniq_plants[i]['opened'] = min([int(x) for x in dict_uniq_plants[i]['operating_year']])
        if dict_uniq_plants[i]['net_generation'] > 0 and 'lat' in dict_uniq_plants[i].keys():
            plants.append(dict_uniq_plants[i])

## Dictionary of power plants
dict_plants_all = {}
for i in plants:
    dict_plants_all[i['cd_year']] = i.copy()

## Unique power plants
uniq_plants_all = prep_data.select_uniq_id(plants,'plant_code')

for i in uniq_plants_all:
    i['opened'] = []
    i['retired'] = []
    for y in rng:
        if str(i['plant_code']) + str(y) in dict_plants_all.keys():
            i['opened'].append(dict_plants_all[str(i['plant_code']) + str(y)]['opened'])
            i['retired'].append(y)
            i[f"net_generation_{y}"] = dict_plants_all[str(i['plant_code']) + str(y)]['net_generation']
        else:
            i[f"net_generation_{y}"] = 0
    i['opened'] = min(i['opened'])
    i['retired'] = max(i['retired'])
    i['type'] = 'biomass'
    i['total_generation'] = round(sum([float(i[f"net_generation_{x}"]) for x in rng]),4)
    if i['total_generation'] >= config['power_plant_cap_mwh']:
        i['radius'] = max(config['power_plant_radius_mile'])
    else:
        i['radius'] = min(config['power_plant_radius_mile'])

## Select states based on the config file
plants_select = prep_data.select_state_config(uniq_plants_all,config)

## Panel CSV output
keys = ['plant_code','type','state','lat','lon','nerc_region','opened','retired','total_generation','radius']
with open('./other_data/power_info_biomass.csv', 'w') as fp:
    prep_data.list_dict_panel_power(plants_select,keys,config,fp)

os.system("""
sed -i '1 s/ /_/g; 1 s/(//g; 1 s/)//g; 1 s/?//g; 1 s/\//_/g' ${OTHER}/power_info_biomass.csv
""")
