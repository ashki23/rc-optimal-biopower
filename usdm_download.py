#!/usr/bin/env python

import os
import sys
import json

## Select years from config.json
config = json.load(open(sys.argv[1]))
year = config['year']

for i in year:
    print('************* USDM:', i, '***************')
    for j in range(1,8):
        os.system(f"""
wget -c -nv --adjust-extension https://droughtmonitor.unl.edu/data/shapefiles_m/USDM_{i}080{j}_M.zip -P ${{OTHER}}
rm ${{OTHER}}/USDM*.html
unzip -n ${{OTHER}}/USDM_{i}080*.zip -d ${{OTHER}}/drought_{i}
        """)
