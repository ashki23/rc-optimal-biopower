#!/usr/bin/env python

import os
import sys
import json
import glob

config = json.load(open(sys.argv[1]))
dist = sys.argv[2]
gms_path = './gms_' + str(dist)
gms_files = glob.glob(f"{gms_path}/*.gms")

for gms in gms_files:
    gdi = gms.split("/")[1]
    gnm = gms.split("/")[-1]
    with open(f"{gms}.sh","w") as fl:
        fl.write(f"""#!/bin/bash

#SBATCH -J {gnm[10:-4]}
#SBATCH -p {config['partition']}
#SBATCH -n 4
#SBATCH --mem 16G

module load gams 2> /dev/null

cd {gdi}
echo "Running {gms} ..."
gams {gnm} gdx={gnm}.gdx > {gnm}.out
sleep 1
gdxdump {gnm}.gdx output={gnm}.csv symb=x format=csv
gdxdump {gnm}.gdx output={gnm}.obj symb=z format=csv header={gnm[10:-8]}-{gnm[-7]}-{gnm[-5]}
        """)
    
    ## Run gmas models
    print(f"Running {gms}.sh ...")
    os.system(f"""
    sleep 1
    if [ {config['job_number_max']} -gt 1 ]; then
    JID=$(sbatch --parsable ./{gms}.sh)
    echo ${{JID}} >> ./jobid-gams-{dist}.log
    else . ./{gms}.sh
    fi
    """)
