#!/bin/bash

#SBATCH --job-name=Batch-Runner
#SBATCH --cpus-per-task=8
#SBATCH --mem=64G

echo ============ Local environments ============ $(hostname) $(date)

source environment.sh

echo ============ Checking config file ==========

if ! jq -e . config.json >/dev/null 2>&1; then
echo "Config JSON file is nat valid:"; jq . config.json
return || exit
fi

if [ `jq ."job_number_max" config.json` -gt 1 ]; then
if [ -z $(which sbatch) ]; then
echo "'sbatch' command not found. In 'config.json' assign 1 for 'job_number_max' to proceed in serial."
return || exit
fi
fi

echo ============ Download databases ============ $(hostname) $(date)

if [ ! -f ${OTHER}/power_info_biomass.csv ]; then python power_info_biomass.py config.json || return || exit; fi
if [ ! -f ${OTHER}/power_info_coal.csv ]; then python power_info_coal.py config.json || return || exit; else
echo "*** The 'power_info' files have been found in 'OTHER'. If the current files are outdated, remove them and rerun the 'batch_file' ***"; fi
source download.sh
python usdm_download.py config.json
python prep_data.py config.json || return || exit

echo =============== HTML queries =============== $(hostname) $(date)

## Download forest attributes from FIA online source
python fia_coordinate.py config.json attributes.json power.json

## Run statistical and optimization models
if [ `jq ."job_number_max" config.json` -eq 1 ]; then
source run_model.sh; else
sbatch --dependency=afterok:`tail -qn 1 ${PROJ_HOME}/jobid-*.log | tr '\n' ',' | grep -Po '.*(?=,$)'` run_model.sh; fi
