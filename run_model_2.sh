#!/bin/bash

#SBATCH --job-name=Forecast
#SBATCH --cpus-per-task=4
#SBATCH --mem=64G

## Local environments
source environment.sh
dst=${1}

## Activate R environment
conda deactivate
conda activate ./optimal_r_env

## Feasible solutions
cd gms_${dst}
grep -i "feasible solution" opt_model_*.gms.out > opt_solutions.out
cat opt_solutions.out | grep "Infeasible" | grep -Po "(?<=_model_).*(?=.gms)" > models.out
cat models.out | grep -Po ".*(?=_[0-9]{1}_.*)" > policy.out
cat models.out | grep -Po "_[0-9]{1}_" | tr -d "_" > alpha.out
cat models.out | grep -Po "(?<=_[0-9]{1}_).*" > theta.out
echo "model,casename,alpha,theta" > infeasible_solutions.csv
paste models.out policy.out alpha.out theta.out -d "," >> infeasible_solutions.csv
rm models.out policy.out alpha.out theta.out

echo "casename,alpha,theta,cost" > objectives.csv
for i in *.obj; do
    csn=`head -n 1 $i | tr "-" ","`
    val=`tail -n 1 $i`
    echo "$csn,$val" >> objectives.csv
done
cd ${PROJ_HOME}
sleep 1

echo ========== Forecast analysis with R ======== $(hostname) $(date)

## Run R model
rm forecast_input_${dst}.RData
Rscript forecast_analysis.R ${dst} > gms_${dst}/Routputs_forecast.out
sleep 1
mv Rplots.pdf gms_${dst}/Rplots_fcst_${dst}.pdf
