#!/bin/bash

## Set env variables
source environment.sh
rbtim=$(date +"%Y-%m-%d-%H:%M")
dtim=$(date +"%Y%m-%d%H%M")
echo ============ Rebatch time: ${rbtim}
install -dvp  ${PROJ_HOME}/job-out-power/archive/rebatch-${dtim}

## Remove invalid json files
for w in $(cat ${PROJ_HOME}/job-out-power/warning.txt | grep input | grep -Po "(?<=Warning: ).*(?= is)"); do
rm -v $w
done

## For serial mode
if [ -z $(which sbatch) ]; then
mv ${PROJ_HOME}/job-out-power/*.* ${PROJ_HOME}/job-out-power/archive/rebatch-${dtim}
. ${FIA}/job-power-0.sh > ${PROJ_HOME}/job-out-power/power-0.out
. ${PROJ_HOME}/job-power.sh > ${PROJ_HOME}/job-out-power/output-serial.out
. ${PROJ_HOME}/report-power.sh > ${PROJ_HOME}/report-power-serial-rebatch.out
return
fi

## Remove the old log file
rm ${PROJ_HOME}/jobid-power-rebatch.log

## For parallel: resubmit the failed jobs
for j in $(cat ${PROJ_HOME}/job-out-power/failed.txt); do
mv ${PROJ_HOME}/job-out-power/${j}-*.out ${PROJ_HOME}/job-out-power/archive/rebatch-${dtim}
JID=$(sbatch --parsable ${FIA}/job-${j}.sh)
echo ${JID} >> ${PROJ_HOME}/jobid-power-rebatch.log
done

## Re-extract informations from JSON files
sleep 2
JOBID=$(cat ${PROJ_HOME}/jobid-power-rebatch.log | tr '\n' ',' | grep -Po '.*(?=,$)')
JID=$(sbatch --parsable --dependency=afterok:$(echo ${JOBID}) ${PROJ_HOME}/job-power.sh)

## A new report
sleep 2
mv ${PROJ_HOME}/job-out-power/failed.txt ${PROJ_HOME}/job-out-power/archive/rebatch-${dtim}
mv ${PROJ_HOME}/job-out-power/warning.txt ${PROJ_HOME}/job-out-power/archive/rebatch-${dtim}
mv ${PROJ_HOME}/job-out-power/output-*.out ${PROJ_HOME}/job-out-power/archive/rebatch-${dtim}
sbatch --dependency=afterany:$(echo ${JID}) ${PROJ_HOME}/report-power.sh ${rbtim}
